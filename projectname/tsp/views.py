# views.py in yourappname
import re
import pandas as pd
from django.shortcuts import render, redirect
from django.http import HttpResponse
import warnings

# Ignore all warnings
warnings.filterwarnings("ignore")



def process_csv(request):
    global final_result_df
    if request.method == 'POST' and request.FILES['csv_file']:
        csv_file = request.FILES['csv_file']

        # Read the CSV file using pandas
        df = pd.read_csv(csv_file)
        
        # df=df1[df1['State'] == "Finished"]
        df.dropna(inplace=True)
        
        
        duration_pattern = r'(?:(?P<hours>\d+) hour[s]? )?(?:(?P<minutes>\d+) min[s]? )?(?:(?P<seconds>\d+) sec[s]?)?'

        # Loop through the 'Time taken' values
        for i, value in enumerate(df['Time taken'].values):
            # Apply the regular expression and convert to timedelta
            duration = df['Time taken'].str.extract(duration_pattern, expand=True).iloc[i]
            df.loc[i, 'Time taken'] = pd.Timedelta(
                hours=int(duration['hours']) if not pd.isna(duration['hours']) else 0,
                minutes=int(duration['minutes']) if not pd.isna(duration['minutes']) else 0,
                seconds=int(duration['seconds']) if not pd.isna(duration['seconds']) else 0
            )

        # Filter rows where duration is greater than 15 minutes
        df1 = df[df['Time taken'] > pd.Timedelta(minutes=10)]

        # Find the column that contains 'Grade'
        matching_columns = [col for col in df1.columns if 'Grade' in col]


        final_result_df = pd.DataFrame()

        # Extract the values along with the column name
        for col_name in matching_columns:
            # Extract grade and numeric value using regex
            match = re.match(r'(.+)/(\d+\.\d+)', col_name)
            if match:
                grade, numeric_value = match.groups()

                # Create a new DataFrame with selected columns
                df1 = df1[['First name', 'Email address', 'Time taken', col_name]]

                # Convert 'Grade/20.00' column to numeric, handling errors by coercing to NaN
                df1[col_name] = pd.to_numeric(df1[col_name], errors='coerce')

                # Drop rows with NaN values in both 'Email address' and 'Grade' columns
                df1 = df1.dropna(subset=['Email address', col_name])

                # Find the index of the maximum grade for each email
                idx = df1.groupby('Email address')[col_name].idxmax()

                # Use loc to get the rows corresponding to the maximum grades
                result_df = df1.loc[idx].reset_index(drop=True)

                # Rename the 'Grade' column
                result_df.rename(columns={col_name: f"{grade}/{numeric_value}"}, inplace=True)

                # Calculate average score and number of attempts for each email address
                result_df['Average'] = df1.groupby('Email address')[col_name].mean().values
                result_df['Average']=result_df['Average'].round()
                result_df['Number of attempts'] = df1.groupby('Email address')[col_name].count().values

               
                
                # Extract hours, minutes, and seconds using regular expression
                

                result_df['Result'] = result_df['Average'] * result_df['Number of attempts']

                # Calculate percentiles using the specified formula
                result_df['Percentile'] = (result_df['Result'].rank() - 1) / (len(result_df)-1) * 100

                # Sort the DataFrame by the 'Percentile' column in descending order
                result_df = result_df.sort_values(by='Percentile', ascending=False).reset_index(drop=True)

                #Round values
                result_df['Percentile'] = result_df['Percentile'].round()
                
                 # Concatenate the results to the final DataFrame
                final_result_df = pd.concat([final_result_df, result_df], axis=1)

       # print(result_df)

        return render(request, 'index.html', {'final_result_df':final_result_df})
   
    return render(request, 'demo.html')



def convert_to_csv(request):
    # Convert DataFrame to CSV and set up response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Percentile_output.csv"'

    # Write DataFrame to response
    final_result_df.to_csv(path_or_buf=response, index=False, encoding='utf-8')

    return response



def process_csv_1(request):
    global final_result_df
    
    if request.method == 'POST' and request.FILES['csv_file_1']:
        # Get the CSV path from the submitted form
        csv_path1 = request.FILES['csv_file_1']

        # Read the CSV file into a DataFrame
        df1 = pd.read_csv(csv_path1)

        # Your existing processing logic goes here...
        columns_to_extract = [col for col in df1.columns if col.startswith('Q. ')]

        # Initialize an empty DataFrame to store the extracted columns
        extracted_columns_df = pd.DataFrame()

        # Also, include the other specified columns
        extracted_columns_df = df1[['First name', 'Email address', 'State', 'Time taken', 'Grade/20.00']]

        # Iterate through each column and extract the desired columns
        for col_name in columns_to_extract:
            extracted_columns_df[col_name] = df1[col_name]
            extracted_columns_df[col_name] = pd.to_numeric(extracted_columns_df[col_name], errors='coerce')

        extracted_columns_df['Ages'] = extracted_columns_df.iloc[:, 5:10].sum(axis=1) / 1.00
        extracted_columns_df['Percentages'] = extracted_columns_df.iloc[:, 10:20].sum(axis=1) / 1.00
        extracted_columns_df['Relationships'] = extracted_columns_df.iloc[:, 20:25].sum(axis=1) / 1.00


        # Extract hours, minutes, and seconds using regular expression
        duration_pattern = r'(?:(?P<hours>\d+) hour[s]? )?(?:(?P<minutes>\d+) min[s]? )?(?:(?P<seconds>\d+) sec[s]?)?'

        for i, value in enumerate(extracted_columns_df['Time taken'].values):
            # Convert to string and then apply the regular expression
            time_str = str(extracted_columns_df['Time taken'].values[i])
            duration = pd.Series(time_str).str.extract(duration_pattern, expand=True).iloc[0]
            
            # Convert to timedelta
            extracted_columns_df.loc[i, 'Time taken'] = pd.Timedelta(
                hours=int(duration['hours']) if not pd.isna(duration['hours']) else 0,
                minutes=int(duration['minutes']) if not pd.isna(duration['minutes']) else 0,
                seconds=int(duration['seconds']) if not pd.isna(duration['seconds']) else 0
            )

        # Filter rows where duration is greater than 15 minutes
        extracted_columns_df = extracted_columns_df[extracted_columns_df['Time taken'] > pd.Timedelta(minutes=15)]

        # final_df = extracted_columns_df[['First name', 'Email address', 'State', 'Time taken', 'Grade/20.00', 'Ages', 'Percentages', 'Relationships']]

        # Sort the DataFrame based on 'First name' column
        final_df = extracted_columns_df.sort_values(by='First name', ascending=True)

        matching_columns = [col for col in final_df.columns if 'Grade' in col]

        # Initialize an empty DataFrame to store the results
        final_result_df = pd.DataFrame()

        # Extract the values along with the column name
        for col_name in matching_columns:
            # Extract grade and numeric value using regex
            match = re.match(r'(.+)/(\d+\.\d+)', col_name)
            if match:
                grade, numeric_value = match.groups()

                # Create a new DataFrame with selected columns
                df = final_df[['First name', 'Email address', 'Time taken', col_name, 'Ages', 'Percentages', 'Relationships']]

                # Convert 'Grade/20.00' column to numeric, handling errors by coercing to NaN
                df[col_name] = pd.to_numeric(df[col_name], errors='coerce')

                # Drop rows with NaN values in both 'Email address' and 'Grade' columns
                df = df.dropna(subset=['Email address', col_name])

                # Find the index of the maximum grade for each email
                idx = df.groupby('Email address')[col_name].idxmax()

                # Use loc to get the rows corresponding to the maximum grades
                result_df = df.loc[idx].reset_index(drop=True)

                # Rename the 'Grade' column
                result_df.rename(columns={col_name: f"{grade}/{numeric_value}"}, inplace=True)
            
        # Render the template with the context
        return render(request, 'index2.html', {'result_df': result_df})

    # Render the form if it's a GET request
    return render(request, 'demo1.html')


def convert_to_csv_1(request):
    # Convert DataFrame to CSV and set up response
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="topic_wise_output.csv"'

    # Write DataFrame to response
    final_result_df.to_csv(path_or_buf=response, index=False, encoding='utf-8')

    return response



