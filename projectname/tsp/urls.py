# urls.py in yourappname
from django.urls import path
from .views import process_csv, convert_to_csv, process_csv_1, convert_to_csv_1

urlpatterns = [
    path('', process_csv, name='process_csv'),
    path('convert_to_csv/', convert_to_csv, name='convert_to_csv'),
    # path('process_csv_1/', process_csv_1, name='process_csv_1'),
    # path('convert_to_csv_1/', convert_to_csv_1, name='convert_to_csv_1'),
    
    
]
